import React from "react";
import { View, Text } from "react-native";
import Evenement from "../classes/Records/Evenement";
import ContentView from "./ContentView";
import SectionView from "./SectionView";

interface EvenementDisplayerProps {
  evenement: Evenement;
  debugMode: boolean;
}

export default class EvenementDisplayer extends React.Component<
  EvenementDisplayerProps
> {
  render() {
    return (
      <ContentView>
        <SectionView>
          {this.props.debugMode ? (
            <Text>{this.props.evenement.toString()}</Text>
          ) : (
            <View>
              <Text style={{ fontSize: 14 }}>
                {this.props.evenement.getPrimaryInfo()}
              </Text>
              <Text style={{ fontSize: 12 }}>
                {this.props.evenement.getSecondaryInfo().join("\n")}
              </Text>
            </View>
          )}
        </SectionView>
      </ContentView>
    );
  }
}
