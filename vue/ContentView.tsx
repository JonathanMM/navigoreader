import React from "react";
import { View } from "react-native";

export default class ContentView extends React.Component {
  render() {
    return <View style={{ paddingLeft: 110 }}>{this.props.children}</View>;
  }
}
