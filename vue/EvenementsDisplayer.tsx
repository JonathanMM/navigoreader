import React from "react";
import Title from "./Title";
import { View } from "react-native";
import Evenement from "../classes/Records/Evenement";
import EvenementDisplayer from "./EvenementDisplayer";

interface EvenementsDisplayerProps {
  evenements: Array<Evenement>;
  debugMode: boolean;
}

export default class EvenementsDisplayer extends React.Component<
  EvenementsDisplayerProps
> {
  render() {
    const evenements = this.props.evenements.filter(
      (evenement) => evenement.EventBitmap.Code.rawValue != 0
    );
    if (evenements.length == 0) return <View></View>;
    return (
      <View>
        <Title title="Dernières validations" />
        {evenements.map((evenement, index) => (
          <EvenementDisplayer
            key={evenement.toString() + index.toString()}
            evenement={evenement}
            debugMode={this.props.debugMode}
          />
        ))}
      </View>
    );
  }
}
