import React from "react";
import { Text, View } from "react-native";
import Contrat from "../classes/Records/Contrat";
import ContentView from "./ContentView";

interface ContractDisplayerProps {
  contract: Contrat;
  debugMode: boolean;
}

export default class ContractDisplayer extends React.Component<
  ContractDisplayerProps
> {
  render() {
    return (
      <ContentView>
        {this.props.debugMode ? (
          <Text>{this.props.contract.toString()}</Text>
        ) : (
          <View>
            <Text style={{ fontSize: 14 }}>
              {this.props.contract.ContractBitmap.getPrimaryInfo()}
            </Text>
            <Text style={{ fontSize: 12 }}>
              {this.props.contract.ContractBitmap.getSecondaryInfo().join(
                " − "
              )}
            </Text>
          </View>
        )}
      </ContentView>
    );
  }
}
