import React from "react";
import { View, Text, Image, TouchableOpacity } from "react-native";
import ContentView from "./ContentView";
import SectionView from "./SectionView";

interface TitleApplicationProps {
  onPressIcon: () => void;
}
export default class TitleApplication extends React.Component<
  TitleApplicationProps
> {
  render() {
    return (
      <View>
        <View
          style={{
            width: 120,
            height: 120,
            backgroundColor: "#C4C4C4",
            left: 40,
            top: 15,
            zIndex: 3,
            position: "absolute",
            flex: 1,
            alignItems: "center",
            justifyContent: "center",
          }}
        >
          <TouchableOpacity onPress={() => this.props.onPressIcon()}>
            <Image source={require("../assets/img/icone.png")} />
          </TouchableOpacity>
        </View>
        <ContentView>
          <SectionView>
            <Text
              style={{
                marginLeft: 75,
                marginTop: 15,
                fontSize: 48,
                fontFamily: "IBM Plex Sans",
              }}
            >
              navigo{"\n"}reader
            </Text>
          </SectionView>
        </ContentView>
      </View>
    );
  }
}
