import React from "react";
import { Text } from "react-native";
import ContentView from "../ContentView";

export default class NfcNotSupportedDisplayed extends React.Component {
  render() {
    return (
      <ContentView>
        <Text
          style={{
            fontSize: 18,
            textAlign: "justify",
          }}
        >
          Votre appareil ne supporte pas la technologie NFC. Ainsi,
          l'application ne vous sert pas à grand chose, puisque le NFC est
          indispensable pour pouvoir lire un passe.
        </Text>
      </ContentView>
    );
  }
}
