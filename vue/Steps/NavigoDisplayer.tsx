import Navigo from "../../classes/Navigo";
import React from "react";
import { View } from "react-native";
import EnvironmentDisplayer from "../EnvironmentDisplayer";
import ContractsDisplayer from "../ContractsDisplayer";
import EvenementsDisplayer from "../EvenementsDisplayer";
import EvenementsSpeciauxDisplayer from "../EvenementsSpeciauxDisplayer";
import DonneeManquanteDisplayer from "../DonneeManquanteDisplayer";

interface NavigoDisplayerProps {
  navigo: Navigo;
  debugMode: boolean;
}

export default class NavigoDisplayer extends React.Component<NavigoDisplayerProps> {
  render() {
    return (
      <View style={{ flex: 1 }}>
        <EnvironmentDisplayer
          env={this.props.navigo.environnement[0]}
          tag={this.props.navigo.tag}
          debugMode={this.props.debugMode}
        />
        <ContractsDisplayer
          contracts={this.props.navigo.contrat}
          debugMode={this.props.debugMode}
        />
        <EvenementsDisplayer
          evenements={this.props.navigo.evenement}
          debugMode={this.props.debugMode}
        />
        <EvenementsSpeciauxDisplayer
          debugMode={this.props.debugMode}
          evenements={this.props.navigo.specialEvenement}
        />
        <DonneeManquanteDisplayer />
      </View>
    );
  }
}
