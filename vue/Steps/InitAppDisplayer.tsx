import React from "react";
import { Text } from "react-native";
import ContentView from "../ContentView";
import HyperLink from "react-native-hyperlink";

export default class InitAppDisplayer extends React.Component {
  render() {
    const urlProjet = "https://framagit.org/JonathanMM/navigoreader";
    const mailProjet = "navigoreader@nocle.fr";
    const urlMailProjet = "mailto:" + mailProjet;
    return (
      <ContentView>
        <HyperLink
          linkDefault={true}
          linkStyle={{ color: "#00BFF1", fontSize: 18 }}
          linkText={(url) =>
            url === urlProjet
              ? "Crédits et code source"
              : url === urlMailProjet
              ? mailProjet
              : url
          }
        >
          <Text
            style={{
              fontSize: 18,
              textAlign: "justify",
            }}
          >
            Active le NFC et présente ton passe navigo au dos de ton appareil.
            {"\n"}
            {"\n"}
            Cette application ne fait que lire les informations publiques de
            votre passe. Rien n’y sera modifié.{"\n"}
            {"\n"}
            Cette application n’est pas officielle et n’est lié en aucun cas à
            aucune autorité organisatrice de mobilités ou quelconque opérateur.
            {"\n"}
            Une donnée manquante (indiquée comme inconnue) ? Envoyer un e-mail
            avec son numéro à {urlMailProjet}
            {"\n"}
            {"\n"}
            {urlProjet}
          </Text>
        </HyperLink>
      </ContentView>
    );
  }
}
