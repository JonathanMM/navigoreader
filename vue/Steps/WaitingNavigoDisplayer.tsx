import React from "react";
import { Text } from "react-native";
import ContentView from "../ContentView";

export default class WaitingNavigoDisplayer extends React.Component {
  render() {
    return (
      <ContentView>
        <Text
          style={{
            fontSize: 18,
            textAlign: "justify",
          }}
        >
          En attente du passe navigo.{"\n"}
          La technologie NFC doit être activé sur votre appareil.
        </Text>
      </ContentView>
    );
  }
}
