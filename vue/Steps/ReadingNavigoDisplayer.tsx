import React from "react";
import { Text } from "react-native";
import ContentView from "../ContentView";

export default class ReadingNavigoDisplayer extends React.Component {
  render() {
    return (
      <ContentView>
        <Text
          style={{
            fontSize: 18,
            textAlign: "justify",
          }}
        >
          Lecture du passe navigo en cours. Veuillez ne pas bouger la carte.
        </Text>
      </ContentView>
    );
  }
}
