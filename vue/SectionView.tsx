import React from "react";
import { View } from "react-native";

export default class SectionView extends React.Component {
  render() {
    return <View style={{ marginBottom: 15 }}>{this.props.children}</View>;
  }
}
