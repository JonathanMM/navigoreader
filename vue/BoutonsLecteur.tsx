import React from "react";
import { View } from "react-native";
import ContentView from "./ContentView";
import BoutonLecteur from "./BoutonLecteur";

interface BoutonsLecteurProps {
  lirePasse: () => void;
  remiseZero: () => void;
}

export default class BoutonsLecteur extends React.Component<
  BoutonsLecteurProps
> {
  render() {
    return (
      <ContentView>
        <View style={{ flex: 1, flexDirection: "row" }}>
          <BoutonLecteur
            title="Lire un passe"
            action={() => this.props.lirePasse()}
          />
          <BoutonLecteur
            title="Remise à zéro"
            action={() => this.props.remiseZero()}
          />
        </View>
      </ContentView>
    );
  }
}
