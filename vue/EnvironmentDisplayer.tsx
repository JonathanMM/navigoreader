import React from "react";
import Environnement from "../classes/Records/Environnement";
import { View, Text } from "react-native";
import Tag from "../classes/Data/Tag";
import Tools from "../classes/Records/Tools";
import ContentView from "./ContentView";
import SectionView from "./SectionView";

interface EnvironmentDisplayerProps {
  env: Environnement;
  tag: Tag;
  debugMode: boolean;
}

export default class EnvironmentDisplayer extends React.Component<
  EnvironmentDisplayerProps
> {
  private readonly _tools: Tools = new Tools();

  render() {
    return (
      <ContentView>
        <SectionView>
          <Text style={{ fontSize: 18 }}>
            Passe navigo n°{this._tools.getNumeroPasse(this.props.tag.id)}
          </Text>
          {this.props.debugMode ? (
            <Text>{this.props.env.toString()}</Text>
          ) : (
            <Text style={{ fontSize: 18 }}>
              {this.props.env.MainBitmap.ValidityEndDate.Value
                ? "Expire le " +
                  this.props.env.MainBitmap.ValidityEndDate.getFormatedValue()
                : ""}
            </Text>
          )}
        </SectionView>
      </ContentView>
    );
  }
}
