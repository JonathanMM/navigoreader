import React from "react";
import Title from "./Title";
import SectionView from "./SectionView";
import HyperLink from "react-native-hyperlink";
import { Text } from "react-native";
import ContentView from "./ContentView";

export default class DonneeManquanteDisplayer extends React.Component {
  render() {
    const mailProjet = "navigoreader@nocle.fr";
    const urlMailProjet = "mailto:" + mailProjet;
    return (
      <SectionView>
        <Title title="Donnée manquante ?" />
        <ContentView>
          <HyperLink
            linkDefault={true}
            linkStyle={{ color: "#00BFF1", fontSize: 14 }}
            linkText={(url) => (url === urlMailProjet ? mailProjet : url)}
          >
            <Text>
              Une donnée manquante (indiquée comme étant inconnue) ? Aidez les
              autres utilisateurs en indiquant à quoi correspond son numéro par
              e-mail à {urlMailProjet}.
            </Text>
          </HyperLink>
        </ContentView>
      </SectionView>
    );
  }
}
