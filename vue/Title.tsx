import { View, Text, TouchableOpacity } from "react-native";
import React from "react";
import ContentView from "./ContentView";

interface TitleProps {
  title: string;
}

export default class Title extends React.Component<TitleProps> {
  render() {
    return (
      <View>
        <View
          style={{
            width: 55,
            height: 25,
            backgroundColor: "#C4C4C4",
            left: 45,
            zIndex: 3,
            position: "absolute",
          }}
        ></View>
        <ContentView>
          <Text
            style={{
              marginTop: -2,
              fontSize: 20,
              fontWeight: "bold",
            }}
          >
            {this.props.title}
          </Text>
        </ContentView>
      </View>
    );
  }
}
