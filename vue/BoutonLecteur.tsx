import React from "react";
import { View, Button } from "react-native";

interface BoutonLecteurProps {
  title: string;
  action: () => void;
}

export default class BoutonLecteur extends React.Component<BoutonLecteurProps> {
  render() {
    return (
      <View style={{ margin: 5 }}>
        <Button
          title={this.props.title}
          color="#00BFF1"
          onPress={this.props.action}
        ></Button>
      </View>
    );
  }
}
