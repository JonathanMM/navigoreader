import Contrat from "../classes/Records/Contrat";
import React from "react";
import ContractDisplayer from "./ContractDisplayer";
import Title from "./Title";
import { View } from "react-native";
import SectionView from "./SectionView";

interface ContractsDisplayerProps {
  contracts: Array<Contrat>;
  debugMode: boolean;
}

export default class ContractsDisplayer extends React.Component<
  ContractsDisplayerProps
> {
  render() {
    const contracts = this.props.contracts.filter(
      (contract) =>
        contract.ContractBitmap.SerialNumber.rawValue != -1 &&
        contract.ContractBitmap.Tarif.rawValue != 0
    );
    if (contracts.length == 0) return <View></View>;
    return (
      <SectionView>
        <Title title="Contrats" />
        {contracts.map((contract) => (
          <ContractDisplayer
            key={contract.ContractBitmap.SerialNumber.rawValue}
            contract={contract}
            debugMode={this.props.debugMode}
          />
        ))}
      </SectionView>
    );
  }
}
