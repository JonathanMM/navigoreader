import Propriete from "./Propriete";
import Tarif from "../Data/Tarif";

export default class TarifContractPropriete extends Propriete {
  public Value: string | undefined;
  private _tarifProvider: Tarif = new Tarif();

  public constructor(nom: string, taille?: number) {
    super(nom, taille ?? 16);
  }

  public setValue(value: number) {
    this.rawValue = value;
    this.Value = this.extractTarif();
  }

  public extractTarif(): string {
    return this._tarifProvider.getTarif(this.rawValue);
  }

  public getFormatedValue(): string {
    if (this.Value == undefined) return this.rawValue.toString();

    return this.Value;
  }
}
