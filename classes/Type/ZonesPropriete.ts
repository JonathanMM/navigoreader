import Propriete from "./Propriete";

export default class ZonesPropriete extends Propriete {
  public Value: string | undefined;

  public constructor(nom: string, taille?: number) {
    super(nom, taille ?? 8);
  }

  public setValue(value: number) {
    this.rawValue = value;
    this.Value = this.extractZones();
  }

  public extractZones(): string {
    let zones = new Array<number>();
    let value = this.rawValue;
    for (let i = 0; i < this.taille; i++) {
      let currentZone = value % 2;
      if (currentZone == 1) zones.push(i + 1);
      value >>= 1;
    }

    if (zones.length == 0) return "-";

    zones.sort();

    let zonesHumain = new Array<string>();
    let minZone = zones[0];
    let maxZone = minZone;
    let precedenteZone = zones[0];
    for (let i = 1; i < zones.length; i++) {
      let zone = zones[i];
      if (zone != precedenteZone + 1) {
        if (minZone == maxZone) zonesHumain.push(minZone.toString());
        else zonesHumain.push(minZone + "-" + maxZone);
        minZone = zone;
      } else maxZone = precedenteZone;

      precedenteZone = zone;
    }

    if (minZone == precedenteZone) zonesHumain.push(minZone.toString());
    else zonesHumain.push(minZone + "-" + precedenteZone);

    return zonesHumain.join(";");
  }

  public getFormatedValue(): string {
    if (this.Value == undefined) return this.rawValue.toString();

    return this.Value;
  }
}
