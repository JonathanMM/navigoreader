import Propriete from "./Propriete";
import EventResult from "../Data/EventResult";

export default class EventResultPropriete extends Propriete {
  public Value: string | undefined;
  private _eventResultProvider: EventResult = new EventResult();

  public constructor(nom: string, taille?: number) {
    super(nom, taille ?? 8);
  }

  public setValue(value: number) {
    this.rawValue = value;
    this.Value = this.extractResultEvenement();
  }

  public extractResultEvenement(): string {
    return this._eventResultProvider.getEventResult(this.rawValue);
  }

  public getFormatedValue(): string {
    if (this.Value == undefined) return this.rawValue.toString();

    return this.Value;
  }
}
