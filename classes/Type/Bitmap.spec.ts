import Bitmap from "./Bitmap";
import Propriete from "./Propriete";

describe("Bitmap", () => {
  describe("getEnabledProperties", () => {
    it("Bitmap with 1 property enabled", () => {
      let bitmap = new Bitmap("foo", 1);
      let property = new Propriete("bar", 1);
      bitmap.proprietes.push(property);

      bitmap.setValue(0b1);

      let sut = bitmap.getEnabledProperties();

      expect(sut.length).toBe(1);
      expect(sut[0]).toBe(property);
    });

    it("Bitmap with 1 property disabled", () => {
      let bitmap = new Bitmap("foo", 1);
      let property = new Propriete("bar", 1);
      bitmap.proprietes.push(property);

      bitmap.setValue(0b0);

      let sut = bitmap.getEnabledProperties();

      expect(sut.length).toBe(0);
    });

    it("Bitmap with 2 property enabled", () => {
      let bitmap = new Bitmap("foo", 2);
      let property = new Propriete("bar", 1);
      let property2 = new Propriete("bar2", 1);
      bitmap.proprietes.push(property);
      bitmap.proprietes.push(property2);

      bitmap.setValue(0b11);

      let sut = bitmap.getEnabledProperties();

      expect(sut.length).toBe(2);
      expect(sut[0]).toBe(property);
      expect(sut[1]).toBe(property2);
    });

    it("Bitmap with 2 property, and only one enabled", () => {
      let bitmap = new Bitmap("foo", 2);
      let property = new Propriete("bar", 1);
      let property2 = new Propriete("bar2", 1);
      bitmap.proprietes.push(property);
      bitmap.proprietes.push(property2);

      bitmap.setValue(0b10);

      let sut = bitmap.getEnabledProperties();

      expect(sut.length).toBe(1);
      expect(sut[0]).toBe(property2);
    });

    it("Bitmap with 4 property, and few enabled", () => {
      let bitmap = new Bitmap("foo", 4);
      let property = new Propriete("bar", 1);
      let property2 = new Propriete("bar2", 1);
      let property3 = new Propriete("bar3", 1);
      let property4 = new Propriete("bar4", 1);
      bitmap.proprietes.push(property);
      bitmap.proprietes.push(property2);
      bitmap.proprietes.push(property3);
      bitmap.proprietes.push(property4);

      bitmap.setValue(0b1011);

      let sut = bitmap.getEnabledProperties();

      expect(sut.length).toBe(3);
      expect(sut[0]).toBe(property);
      expect(sut[1]).toBe(property2);
      expect(sut[2]).toBe(property4);
    });
  });
});
