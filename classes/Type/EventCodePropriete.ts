import Propriete from "./Propriete";
import EventCode from "../Data/EventCode";

export default class EventCodePropriete extends Propriete {
  public Value: string | undefined;
  private _eventCodeProvider: EventCode = new EventCode();

  public constructor(nom: string, taille?: number) {
    super(nom, taille ?? 8);
  }

  public setValue(value: number) {
    this.rawValue = value;
    this.Value = this.extractCodeEvenement();
  }

  public extractCodeEvenement(): string {
    return this._eventCodeProvider.getEventCode(this.rawValue);
  }

  public getFormatedValue(): string {
    if (this.Value == undefined) return this.rawValue.toString();

    return this.Value;
  }
}
