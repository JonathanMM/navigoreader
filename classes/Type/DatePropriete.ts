import Propriete from "./Propriete";
import Tools from "../Records/Tools";

export default class DatePropriete extends Propriete {
  public Value: Date | undefined;
  public static origin: Date = new Date(Date.UTC(1997, 0, 1)); //1er janvier 1997, origine
  private readonly _tools: Tools = new Tools();

  public constructor(nom: string, taille?: number) {
    super(nom, taille ?? 14);
  }

  public setValue(value: number) {
    this.rawValue = value;
    this.Value = this.extractDate();
  }

  public extractDate(): Date {
    let origin = new Date(DatePropriete.origin);
    origin.setUTCDate(origin.getUTCDate() + this.rawValue);
    return origin;
  }

  public getFormatedValue(): string {
    if (this.Value == undefined) return "";

    return (
      this._tools.putZero(this.Value.getUTCDate()) +
      "/" +
      this._tools.putZero(this.Value.getUTCMonth() + 1) +
      "/" +
      this.Value.getUTCFullYear()
    );
  }
}
