import Propriete from "./Propriete";
import Tools from "../Records/Tools";

export default class TimePropriete extends Propriete {
  public Value: Date | undefined;
  private readonly _tools: Tools = new Tools();

  public constructor(nom: string, taille?: number) {
    super(nom, taille ?? 11);
  }

  public setValue(value: number) {
    this.rawValue = value;
    this.Value = this.extractTime();
  }

  public extractTime(): Date {
    let origin = new Date(Date.UTC(0, 0, 0, 0, 0, 0));
    origin.setUTCMinutes(origin.getUTCMinutes() + this.rawValue);
    return origin;
  }

  public getFormatedValue(): string {
    if (this.Value == undefined) return "";

    return (
      this._tools.putZero(this.Value.getUTCHours()) +
      "h" +
      this._tools.putZero(this.Value.getUTCMinutes())
    );
  }
}
