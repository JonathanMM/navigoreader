import Propriete from "./Propriete";
import Pays from "../Data/Pays";
import Reseau from "../Data/Reseau";

export default class NetworkPropriete extends Propriete {
  public Value: string | undefined;
  private _paysProvider: Pays = new Pays();
  private _reseauProvider: Reseau = new Reseau();

  public constructor(nom: string, taille?: number) {
    super(nom, taille ?? 24);
  }

  public setValue(value: number) {
    this.rawValue = value;
    this.Value = this.extractNetwork();
  }

  public extractNetwork(): string {
    // On a 24 bits, soit 6 groupes de 4. Chaque groupe correspond à un chiffre (entre 0 et 9), les 3 premiers au pays, les 3 derniers au réseau
    let currentValue = 0;
    const nbBitsParChiffre = 4;
    let resteBitAPrendre = nbBitsParChiffre;
    let nbChiffresAPrendre = 6;

    let rawValue = this.rawValue;

    let bufferBinaire = new Array<number>();
    for (let i = 0; i < nbBitsParChiffre * nbChiffresAPrendre; i++) {
      let currentFlag = rawValue % 2;
      bufferBinaire.push(currentFlag);
      rawValue >>= 1;
    }

    bufferBinaire.reverse();

    let chiffres = new Array<number>();

    while (resteBitAPrendre > 0 && bufferBinaire.length > 0) {
      currentValue <<= 1;
      let valeur = bufferBinaire.shift();
      if (valeur !== undefined) currentValue += valeur;
      resteBitAPrendre--;

      if (resteBitAPrendre == 0) {
        chiffres.push(currentValue);

        currentValue = 0;
        resteBitAPrendre = nbBitsParChiffre;
      }
    }

    if (chiffres.length != nbChiffresAPrendre) {
      console.warn("Format incorrect du Network");
      return "";
    }

    const codePays = chiffres[0] * 100 + chiffres[1] * 10 + chiffres[2];
    const codeReseau = chiffres[3] * 100 + chiffres[4] * 10 + chiffres[5];
    const nomPays = this._paysProvider.getPays(codePays);
    const nomReseau = this._reseauProvider.getReseau(codePays, codeReseau);

    if (nomPays.length == 0) return "Pays inconnu";
    if (nomReseau.length == 0) return nomPays + " > réseau inconnu";

    return nomPays + " > " + nomReseau;
  }

  public getFormatedValue(): string {
    if (this.Value == undefined) return this.rawValue.toString();

    return this.Value;
  }
}
