import Propriete from "./Propriete";

export default class Bitmap extends Propriete {
  public readonly proprietes: Array<Propriete>;

  public constructor(nom: string, taille: number) {
    super(nom, taille);
    this.proprietes = new Array<Propriete>();
  }

  public getEnabledProperties(): Array<Propriete> {
    // Le bit de la première propriété du tableau est le dernier de la valeur
    let flags = this.rawValue;
    let enabled = new Array<Propriete>();
    //console.debug("enabledProperties", this.proprietes);
    //console.debug("enabledProperties", "value = " + flags);
    for (let i = 0; i < this.taille; i++) {
      let currentFlag = flags % 2;
      //console.debug("flag", flags);
      //console.debug("currentFlag", currentFlag);
      if (currentFlag == 1) enabled.push(this.proprietes[i]);
      flags >>= 1;
    }
    return enabled;
  }

  public toString(): string {
    return (
      "Bitmap(" +
      this.proprietes.map((prop) => prop.toString()).join(", ") +
      ")"
    );
  }
}
