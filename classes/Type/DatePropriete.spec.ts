import DatePropriete from "./DatePropriete";

describe("DatePropriete", () => {
  describe("constructor", () => {
    it("Default value for taille is 14", () => {
      let property = new DatePropriete("foo");

      let sut = property.taille;

      expect(sut).toBe(14);
    });
  });

  describe("extractData", () => {
    it("origin is 1997-01-01", () => {
      let property = new DatePropriete("foo");

      property.setValue(0);

      let sut = property.Value;

      expect(sut?.getUTCFullYear()).toBe(1997);
      expect(sut?.getUTCMonth()).toBe(0);
      expect(sut?.getUTCDate()).toBe(1);
    });

    it("10 days is 1997-01-11", () => {
      let property = new DatePropriete("foo");

      property.setValue(10);

      let sut = property.Value;

      expect(sut?.getUTCFullYear()).toBe(1997);
      expect(sut?.getUTCMonth()).toBe(0);
      expect(sut?.getUTCDate()).toBe(11);
    });

    it("100 days is 1997-04-11", () => {
      let property = new DatePropriete("foo");

      property.setValue(100);

      let sut = property.Value;

      expect(sut?.getUTCFullYear()).toBe(1997);
      expect(sut?.getUTCMonth()).toBe(3);
      expect(sut?.getUTCDate()).toBe(11);
    });

    it("1000 days is 1999-09-23", () => {
      let property = new DatePropriete("foo");

      property.setValue(1000);

      let sut = property.Value;

      expect(sut?.getUTCFullYear()).toBe(1999);
      expect(sut?.getUTCMonth()).toBe(8);
      expect(sut?.getUTCDate()).toBe(28);
    });
  });
});
