import Propriete from "./Propriete";

export default class MontantPropriete extends Propriete {
  public Value: string | undefined;

  public constructor(nom: string, taille?: number) {
    super(nom, taille ?? 16);
  }

  public setValue(value: number) {
    this.rawValue = value;
    this.Value = this._extractMontant();
  }

  private _formatValue(val: number): string {
    return val.toFixed(2).toString().replace(".", ",") + " €";
  }

  private _extractMontant(): string {
    let prix = this.rawValue / 100;
    return this._formatValue(prix);
  }

  public getFormatedValue(): string {
    if (this.Value == undefined) return this.rawValue.toString();

    return this.Value;
  }
}
