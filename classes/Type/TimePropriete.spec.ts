import TimePropriete from "./TimePropriete";

describe("TimePropriete", () => {
  describe("constructor", () => {
    it("Default value for taille is 11", () => {
      let property = new TimePropriete("foo");

      let sut = property.taille;

      expect(sut).toBe(11);
    });
  });

  describe("extractData", () => {
    it("origin is 00h00", () => {
      let property = new TimePropriete("foo");

      property.setValue(0);

      let sut = property.Value;

      expect(sut?.getUTCHours()).toBe(0);
      expect(sut?.getUTCMinutes()).toBe(0);
    });

    it("10 minutes is 00h10", () => {
      let property = new TimePropriete("foo");

      property.setValue(10);

      let sut = property.Value;

      expect(sut?.getUTCHours()).toBe(0);
      expect(sut?.getUTCMinutes()).toBe(10);
    });

    it("100 minutes is 01h40", () => {
      let property = new TimePropriete("foo");

      property.setValue(100);

      let sut = property.Value;

      expect(sut?.getUTCHours()).toBe(1);
      expect(sut?.getUTCMinutes()).toBe(40);
    });

    it("1000 minutes is 16h40", () => {
      let property = new TimePropriete("foo");

      property.setValue(1000);

      let sut = property.Value;

      expect(sut?.getUTCHours()).toBe(16);
      expect(sut?.getUTCMinutes()).toBe(40);
    });
  });
});
