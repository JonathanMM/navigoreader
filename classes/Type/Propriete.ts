export default class Propriete {
  public readonly nom: string;
  public readonly taille: number;
  public rawValue: number = 0;

  public constructor(nom: string, taille: number) {
    this.nom = nom;
    this.taille = taille;
  }

  public setValue(value: number) {
    this.rawValue = value;
  }

  public getFormatedValue(): string {
    return this.rawValue.toString();
  }

  public toString(): string {
    return this.nom + ": " + this.getFormatedValue();
  }
}
