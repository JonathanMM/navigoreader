import { TypedResponse, TypedResponseProvider } from "./Records/TypedResponse";

export enum ResponseStatus {
  None,
  OK,
  RecordNotFound,
  HasRecord,
  KO,
}

export default class ResponseAPDU {
  public readonly raw: Array<number>;
  public readonly status: ResponseStatus;

  public constructor(response: Array<number>) {
    this.raw = response;
    this.status = this.getStatutResponse();
  }

  public static readonly Fail = new ResponseAPDU([]);

  private getStatutResponse(): ResponseStatus {
    if (this.raw.length < 2) return ResponseStatus.KO;

    const typeReponse = this.raw[this.raw.length - 2];
    const codeResponse = this.raw[this.raw.length - 1];

    if (typeReponse == 0x90) return ResponseStatus.OK;
    if (typeReponse == 0xf9) return ResponseStatus.HasRecord;

    if (typeReponse == 0x6a && codeResponse == 0x83)
      return ResponseStatus.RecordNotFound;

    return ResponseStatus.KO;
  }
}
