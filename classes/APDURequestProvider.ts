export enum INS {
  selectFile = 0xa4,
  selectFileArg = 0x08,
  getRecord = 0xb2,
  readMode = 0x04,
}

export default class APDURequestProvider {
  private readonly CLA: number = 0x94;

  public getCommandSelectFile(
    adresseDF: Array<number>,
    adresseEF: Array<number>
  ): Array<number> {
    let commande = [
      this.CLA,
      INS.selectFile,
      INS.selectFileArg,
      0x00, // 0
      adresseDF.length + adresseEF.length, // Taille de la demande
    ];
    adresseDF.forEach((val) => commande.push(val));
    adresseEF.forEach((val) => commande.push(val));
    return commande;
  }

  public getCommandRecord(idRecord: number): Array<number> {
    return [
      this.CLA,
      INS.getRecord, //INS read record
      idRecord, //Id du record
      INS.readMode, //Mode de lecture
      0x00, //Taille du record
    ];
  }
}
