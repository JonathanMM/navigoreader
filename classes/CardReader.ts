import nfcManager from "react-native-nfc-manager";
import ResponseAPDU from "./ResponseAPDU";

export default class CardReader {
  public async sendCommand(command: Array<number>): Promise<ResponseAPDU> {
    console.debug("Request APDU >> ", this.toStringCommand(command));
    /*
if (Platform.OS === 'ios') {
        resp = await NfcManager.sendCommandAPDUIOS([0x00, 0x84, 0x00, 0x00, 0x08]);
      } else {
    */
    return nfcManager
      .transceive(command)
      .then((response: Array<number>) => {
        console.debug("Réponse APDU << ", this.toStringCommand(response));
        return new ResponseAPDU(response);
      })
      .catch(() => ResponseAPDU.Fail);
  }

  public toStringCommand(command: Array<number>): string {
    return command.map((val: number) => val.toString(16)).join("");
  }
}
