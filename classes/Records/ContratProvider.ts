import { TypedResponseProvider } from "./TypedResponse";
import Contrat from "./Contrat";

export default class ContratProvider implements TypedResponseProvider {
  create(response: number[]): Contrat {
    return new Contrat(response);
  }
}
