import { TypedResponse } from "./TypedResponse";
import Tools from "./Tools";
import Propriete from "../Type/Propriete";
import MainBitmap from "../Bitmap/MainBitmap";

export default class Environnement implements TypedResponse {
  public readonly VersionNumber: Propriete;
  public readonly MainBitmap: MainBitmap;

  public constructor(responseInfo: Array<number>) {
    /*
        VersionNumber: 6
        MainBitmap: 7
        */
    this.VersionNumber = new Propriete("VersionNumber", 6);
    this.MainBitmap = new MainBitmap();
    let properties = new Array<Propriete>(this.VersionNumber, this.MainBitmap);

    const tools = new Tools();
    tools.extractProperties(responseInfo, properties);
  }

  public toString(): string {
    return (
      "Version : " +
      this.VersionNumber.getFormatedValue() +
      "\n" +
      this.MainBitmap.toString()
    );
  }
}
