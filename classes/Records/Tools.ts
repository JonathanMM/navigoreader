import Propriete from "../Type/Propriete";
import Bitmap from "../Type/Bitmap";

export default class Tools {
  public extractProperties(
    responseInfo: Array<number>,
    properties: Array<Propriete>
  ): Array<Propriete> {
    let position = 0;
    let tailleReponse = responseInfo.length - 2; //Deux derniers octets de réponse = statut réponse
    let bufferBinaire = new Array<number>();

    let currentProperty = 0;
    let currentValue = 0;
    let resteBitAPrendre = properties[currentProperty].taille;

    while (position < tailleReponse && currentProperty < properties.length) {
      //console.debug("Extract", "Properties length = " + properties.length);
      let element = responseInfo[position];
      bufferBinaire.push((element & 0x80) >> 7);
      bufferBinaire.push((element & 0x40) >> 6);
      bufferBinaire.push((element & 0x20) >> 5);
      bufferBinaire.push((element & 0x10) >> 4);
      bufferBinaire.push((element & 0x08) >> 3);
      bufferBinaire.push((element & 0x04) >> 2);
      bufferBinaire.push((element & 0x02) >> 1);
      bufferBinaire.push((element & 0x01) >> 0);

      while (resteBitAPrendre > 0 && bufferBinaire.length > 0) {
        currentValue <<= 1;
        let valeur = bufferBinaire.shift();
        if (valeur !== undefined) currentValue += valeur;
        resteBitAPrendre--;

        if (resteBitAPrendre == 0) {
          let prop = properties[currentProperty];
          prop.setValue(currentValue);

          if (prop instanceof Bitmap) {
            let bitmap = prop as Bitmap;
            properties.splice(
              currentProperty + 1,
              0,
              ...bitmap.getEnabledProperties()
            );
          }

          currentValue = 0;
          currentProperty++;
          if (currentProperty < properties.length)
            resteBitAPrendre = properties[currentProperty].taille;
        }
      }
      position++;
    }
    return properties;
  }

  public putZero(num: number): string {
    if (num <= 9) return "0" + num;
    return num.toString();
  }

  public getNumeroPasse(numeroRaw: string): string {
    console.debug("getNumeroPasse", numeroRaw);
    if (numeroRaw == null) return "";

    let numero = parseInt(numeroRaw, 16);
    let numStr: string = "";

    numStr += Math.floor(numero / 1000000000);
    numStr += " ";
    numStr += Math.floor(numero / 1000000) % 1000;
    numStr += " ";
    numStr += Math.floor(numero / 1000) % 1000;
    numStr += " ";
    numStr += numero % 1000;
    return numStr;
  }
}
