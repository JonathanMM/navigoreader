import { TypedResponse } from "./TypedResponse";
import Tools from "./Tools";
import Propriete from "../Type/Propriete";
import ContractListBitmap from "../Bitmap/ContractListBitmap";

export default class ContratList implements TypedResponse {
  public readonly BestContractsCounter: Propriete;
  public readonly BestContractBitmap: ContractListBitmap;

  public constructor(responseInfo: Array<number>) {
    /*
    BestContractsCounter: 4,
    BestContractBitmap: 3,
        */

    this.BestContractsCounter = new Propriete("BestContractsCounter", 4);
    this.BestContractBitmap = new ContractListBitmap();

    let properties: Array<Propriete> = [
      this.BestContractsCounter,
      this.BestContractBitmap,
    ];

    const tools = new Tools();
    tools.extractProperties(responseInfo, properties);
  }
}
