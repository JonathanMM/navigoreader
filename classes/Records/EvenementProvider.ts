import { TypedResponseProvider } from "./TypedResponse";
import Evenement from "./Evenement";

export default class EvenementProvider implements TypedResponseProvider {
  create(response: number[]): Evenement {
    return new Evenement(response);
  }
}
