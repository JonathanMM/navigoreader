export interface TypedResponse {}

export interface TypedResponseProvider {
  create(response: Array<number>): TypedResponse;
}
