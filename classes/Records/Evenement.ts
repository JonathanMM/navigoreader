import { TypedResponse } from "./TypedResponse";
import Tools from "./Tools";
import Propriete from "../Type/Propriete";
import DatePropriete from "../Type/DatePropriete";
import TimePropriete from "../Type/TimePropriete";
import EventBitmap from "../Bitmap/EventBitmap";

export default class Evenement implements TypedResponse {
  public readonly DateStamp: DatePropriete;
  public readonly TimeStamp: TimePropriete;
  public readonly EventBitmap: EventBitmap;

  public constructor(responseInfo: Array<number>) {
    /*
    DateStamp: 14,
    TimeStamp: 11,
    EventBitmap: 28,
    */

    this.DateStamp = new DatePropriete("DateStamp", 14);
    this.TimeStamp = new TimePropriete("TimeStamp", 11);
    this.EventBitmap = new EventBitmap();

    let properties = new Array<Propriete>(
      this.DateStamp,
      this.TimeStamp,
      this.EventBitmap
    );

    const tools = new Tools();
    tools.extractProperties(responseInfo, properties);
  }

  public getPrimaryInfo(): string {
    return this.EventBitmap.getPrimaryInfo();
  }

  public getSecondaryInfo(): Array<string> {
    let infos = new Array<string>();

    infos = infos.concat(this.EventBitmap.getSecondaryInfo());

    infos.push(
      "Le " +
        this.DateStamp.getFormatedValue() +
        " à " +
        this.TimeStamp.getFormatedValue()
    );

    return infos;
  }

  public toString(): string {
    return (
      "Date " +
      this.DateStamp.getFormatedValue() +
      " " +
      this.TimeStamp.getFormatedValue() +
      "\n" +
      this.EventBitmap.toString()
    );
  }
}
