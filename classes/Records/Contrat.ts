import { TypedResponse } from "./TypedResponse";
import Tools from "./Tools";
import Propriete from "../Type/Propriete";
import ContractBitmap from "../Bitmap/ContractBitmap";

export default class Contrat implements TypedResponse {
  public readonly ContractBitmap: ContractBitmap;

  public constructor(responseInfo: Array<number>) {
    /*
    ContractBitmap: 20
    */
    this.ContractBitmap = new ContractBitmap();

    let properties = new Array<Propriete>(this.ContractBitmap);

    const tools = new Tools();
    tools.extractProperties(responseInfo, properties);
  }

  public toString(): string {
    return this.ContractBitmap.toString();
  }
}
