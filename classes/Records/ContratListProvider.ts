import { TypedResponseProvider } from "./TypedResponse";
import ContratList from "./ContratList";

export default class ContratListProvider implements TypedResponseProvider {
  create(response: number[]): ContratList {
    return new ContratList(response);
  }
}
