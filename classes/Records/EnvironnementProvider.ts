import { TypedResponseProvider } from "./TypedResponse";
import Environnement from "./Environnement";

export default class EnvironnementProvider implements TypedResponseProvider {
  create(response: number[]): Environnement {
    return new Environnement(response);
  }
}
