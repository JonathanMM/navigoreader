export default class Calypso {
  public getCalypsoDF(): Array<number> {
    return [0x20, 0x00];
  }

  public getCalypsoEF(): Array<Array<number>> {
    return [
      [0x20, 0x01], //Environnement
      [0x20, 0x10], //Événements
      [0x20, 0x20], //Contrats
      [0x20, 0x30], //Liste des contrats
      [0x20, 0x40], //Événements spéciaux
    ];
  }
}

export enum CalypsoEF {
  Environnement,
  Evenements,
  Contrats,
  ListeContrats,
  EvenementsSpeciaux,
}
