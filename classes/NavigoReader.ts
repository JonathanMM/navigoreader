import CardReader from "./CardReader";
import Calypso, { CalypsoEF } from "./Calypso";
import APDURequestProvider from "./APDURequestProvider";
import Navigo from "./Navigo";
import { ResponseStatus } from "./ResponseAPDU";
import { TypedResponseProvider } from "./Records/TypedResponse";
import EnvironnementProvider from "./Records/EnvironnementProvider";
import Environnement from "./Records/Environnement";
import Evenement from "./Records/Evenement";
import EvenementProvider from "./Records/EvenementProvider";
import Contrat from "./Records/Contrat";
import ContratProvider from "./Records/ContratProvider";
import ContratListProvider from "./Records/ContratListProvider";
import ContratList from "./Records/ContratList";
import Tag from "./Data/Tag";

export default class NavigoReader {
  private readonly _cardReader: CardReader;
  private readonly _calypso: Calypso;
  private readonly _apduRequestProvider: APDURequestProvider;

  public constructor() {
    this._cardReader = new CardReader();
    this._calypso = new Calypso();
    this._apduRequestProvider = new APDURequestProvider();
  }

  public async readNavigo(tag: Tag): Promise<Navigo | null> {
    const navigo = new Navigo(tag);
    let currentEF = CalypsoEF.Environnement;
    let provider: TypedResponseProvider;
    // On va lire toutes les sections une par une
    for (let ef of this._calypso.getCalypsoEF()) {
      switch (currentEF) {
        case CalypsoEF.Environnement:
          provider = new EnvironnementProvider();
          break;
        case CalypsoEF.Evenements:
        case CalypsoEF.EvenementsSpeciaux:
          provider = new EvenementProvider();
          break;
        case CalypsoEF.Contrats:
          provider = new ContratProvider();
          break;
        case CalypsoEF.ListeContrats:
          provider = new ContratListProvider();
          break;
      }

      let response = await this._cardReader.sendCommand(
        this._apduRequestProvider.getCommandSelectFile(
          this._calypso.getCalypsoDF(),
          ef
        )
      );

      if (response.status == ResponseStatus.KO) {
        console.warn("Réponse", "Réponse obtenue incorrecte");
        return null;
      }

      console.info("Réponse", "EF obtenu");

      //Et dedans, on doit lire les records un à un
      let lastResponseStatus = ResponseStatus.None;
      let idRecord = 1;

      while (lastResponseStatus !== ResponseStatus.RecordNotFound) {
        let responseRecord = await this._cardReader.sendCommand(
          this._apduRequestProvider.getCommandRecord(idRecord)
        );

        if (responseRecord.status == ResponseStatus.KO) {
          console.warn("Réponse", "Réponse obtenue incorrecte");
          return null;
        }

        if (responseRecord.status != ResponseStatus.RecordNotFound) {
          switch (currentEF) {
            case CalypsoEF.Environnement:
              navigo.environnement.push(
                provider.create(responseRecord.raw) as Environnement
              );
              break;
            case CalypsoEF.Evenements:
              navigo.evenement.push(
                provider.create(responseRecord.raw) as Evenement
              );
              break;
            case CalypsoEF.Contrats:
              navigo.contrat.push(
                provider.create(responseRecord.raw) as Contrat
              );
              break;
            case CalypsoEF.ListeContrats:
              navigo.contratList.push(
                provider.create(responseRecord.raw) as ContratList
              );
              break;
            case CalypsoEF.EvenementsSpeciaux:
              navigo.specialEvenement.push(
                provider.create(responseRecord.raw) as Evenement
              );
              break;
          }
        }

        lastResponseStatus = responseRecord.status;
        idRecord++;
      }

      switch (currentEF) {
        case CalypsoEF.Environnement:
          currentEF = CalypsoEF.Evenements;
          break;
        case CalypsoEF.Evenements:
          currentEF = CalypsoEF.Contrats;
          break;
        case CalypsoEF.Contrats:
          currentEF = CalypsoEF.ListeContrats;
          break;
        case CalypsoEF.ListeContrats:
          currentEF = CalypsoEF.EvenementsSpeciaux;
          break;
      }
    }

    return navigo;
  }
}
