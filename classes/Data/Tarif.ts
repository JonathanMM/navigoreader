export enum CodeTarif {
  Navigo = 1,
  NavigoAnnuel = 2,
  ImaginR = 4,
  ImaginRBis = 5,
  LibertePlus = 4096,
  TicketTPlus = 20480,
  TicketTPlusDemiTarif = 20496,
  SolidariteTransport = 32771,
}

export default class Tarif {
  public getTarif(codeTarif: number): string {
    switch (codeTarif) {
      case CodeTarif.Navigo:
        return "Navigo Jour/Semaine/Mois";
      case CodeTarif.NavigoAnnuel:
        return "Navigo Annuel";
      case CodeTarif.ImaginR:
      case CodeTarif.ImaginRBis:
        return "Imagin'R";
      case CodeTarif.LibertePlus:
        return "Navigo Liberté +";
      case CodeTarif.TicketTPlus:
        return "Ticket t+";
      case CodeTarif.TicketTPlusDemiTarif:
        return "Ticket t+ (demi tarif)";
      case CodeTarif.SolidariteTransport:
        return "Solidarité Transport";
      default:
        console.warn("Code tarif inconnu : " + codeTarif);
        return "Tarif inconnu (" + codeTarif + ")";
    }
  }
}
