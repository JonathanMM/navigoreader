import Environnement from "./Records/Environnement";
import Evenement from "./Records/Evenement";
import Contrat from "./Records/Contrat";
import ContratList from "./Records/ContratList";
import Tag from "./Data/Tag";

export default class Navigo {
  public tag: Tag;
  public environnement: Array<Environnement>;
  public evenement: Array<Evenement>;
  public contrat: Array<Contrat>;
  public contratList: Array<ContratList>;
  public specialEvenement: Array<Evenement>;

  public constructor(tag: Tag) {
    this.tag = tag;
    this.environnement = new Array<Environnement>();
    this.evenement = new Array<Evenement>();
    this.contrat = new Array<Contrat>();
    this.contratList = new Array<ContratList>();
    this.specialEvenement = new Array<Evenement>();
  }

  public toString(): string {
    return (
      "Navigo {" +
      "environnement: [" +
      this.environnement.map((item) => item.toString()).join(", ") +
      "]," +
      "evenement: [" +
      this.evenement.map((item) => item.toString()).join(", ") +
      "]," +
      "contrat: [" +
      this.contrat.map((item) => item.toString()).join(", ") +
      "]," +
      "contratList: [" +
      this.contratList.map((item) => item.toString()).join(", ") +
      "]," +
      "specialEvenement: [" +
      this.specialEvenement.map((item) => item.toString()).join(", ") +
      "]," +
      "}"
    );
  }
}
