import Bitmap from "../Type/Bitmap";
import Propriete from "../Type/Propriete";

export default class JourneyDataBitmap extends Bitmap {
  public readonly Origin: Propriete;
  public readonly Destination: Propriete;
  public readonly RouteNumbers: Propriete;
  public readonly RouteVariants: Propriete;
  public readonly Run: Propriete;
  public readonly Via: Propriete;
  public readonly Distance: Propriete;
  public readonly Interchanges: Propriete;

  public constructor() {
    super("JourneyDataBitmap", 8);

    /*
    //Origin: 16,
    //Destination: 16,
    //RouteNumbers: 16,
    //RouteVariants: 8,
    //Run: 16,
    //Via: 16, 
    //Distance: 16,
    //Interchanges: 8,
    */

    this.Origin = new Propriete("Origin", 16);
    this.Destination = new Propriete("Destination", 16);
    this.RouteNumbers = new Propriete("RouteNumbers", 16);
    this.RouteVariants = new Propriete("RouteVariants", 8);
    this.Run = new Propriete("Run", 16);
    this.Via = new Propriete("Via", 16);
    this.Distance = new Propriete("Distance", 16);
    this.Interchanges = new Propriete("Interchanges", 8);

    this.proprietes.push(this.Origin);
    this.proprietes.push(this.Destination);
    this.proprietes.push(this.RouteNumbers);
    this.proprietes.push(this.RouteVariants);
    this.proprietes.push(this.Run);
    this.proprietes.push(this.Via);
    this.proprietes.push(this.Distance);
    this.proprietes.push(this.Interchanges);
  }

  public toString(): string {
    return "Des informations de validités sont présentes";
  }
}
