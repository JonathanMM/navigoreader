import Bitmap from "../Type/Bitmap";
import Propriete from "../Type/Propriete";
import NetworkPropriete from "../Type/NetworkPropriete";

export default class ContractListBitmap extends Bitmap {
  public readonly NetworkId: NetworkPropriete;
  public readonly Tarif: Propriete;
  public readonly Pointer: Propriete;

  public constructor() {
    super("BestContractBitmap", 3);

    /*
    NetworkId: 24,
    Tarif: 16,
    Pointer: 5
    */

    this.NetworkId = new NetworkPropriete("NetworkId", 24);
    this.Tarif = new Propriete("Tarif", 16);
    this.Pointer = new Propriete("Pointer", 5);

    this.proprietes.push(this.NetworkId);
    this.proprietes.push(this.Tarif);
    this.proprietes.push(this.Pointer);
  }
}
