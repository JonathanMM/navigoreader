import Bitmap from "../Type/Bitmap";
import Propriete from "../Type/Propriete";

export default class ContractRestrictBitmap extends Bitmap {
  public readonly Start: Propriete;
  public readonly End: Propriete;
  public readonly Day: Propriete;
  public readonly TimeCode: Propriete;
  public readonly Code: Propriete;
  public readonly Product: Propriete;
  public readonly Location: Propriete;

  public constructor() {
    super("ContractRestrictBitmap", 7);

    /*
    //RestrictStart: 11,
    //RestrictEnd: 11,
    //RestrictDay: 8,
    //RestrictTimeCode: 8,
    //RestrictCode: 8,
    //RestrictProduct: 16,
    //RestrictLocation: 16,
    */

    this.Start = new Propriete("Start", 11);
    this.End = new Propriete("End", 11);
    this.Day = new Propriete("Day", 8);
    this.TimeCode = new Propriete("TimeCode", 8);
    this.Code = new Propriete("TimeCode", 8);
    this.Product = new Propriete("Product", 16);
    this.Location = new Propriete("Location", 16);

    this.proprietes.push(this.Start);
    this.proprietes.push(this.End);
    this.proprietes.push(this.Day);
    this.proprietes.push(this.TimeCode);
    this.proprietes.push(this.Code);
    this.proprietes.push(this.Product);
    this.proprietes.push(this.Location);
  }

  public toString(): string {
    return "Une restriction est présente";
  }
}
