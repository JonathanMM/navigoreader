import Bitmap from "../Type/Bitmap";
import Propriete from "../Type/Propriete";

export default class PassengerInfoBitmap extends Bitmap {
  public readonly Class: Propriete;
  public readonly Total: Propriete;

  public constructor() {
    super("PassengerInfoBitmap", 2);

    /*
    //PassengerClass: 8,
    //PassengerTotal: 8,
    */

    this.Class = new Propriete("Class", 6);
    this.Total = new Propriete("Total", 32);

    this.proprietes.push(this.Class);
    this.proprietes.push(this.Total);
  }

  public toString(): string {
    return (
      "Classe : " +
      this.Class.getFormatedValue() +
      "\n" +
      "Nombre total de voyageurs : " +
      this.Total.getFormatedValue()
    );
  }
}
