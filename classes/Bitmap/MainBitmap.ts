import Bitmap from "../Type/Bitmap";
import Propriete from "../Type/Propriete";
import DatePropriete from "../Type/DatePropriete";
import DataBitmap from "./DataBitmap";
import NetworkPropriete from "../Type/NetworkPropriete";

export default class MainBitmap extends Bitmap {
  public readonly NetworkId: NetworkPropriete;
  public readonly IssuerId: Propriete;
  public readonly ValidityEndDate: DatePropriete;
  public readonly PayMethod: Propriete;
  public readonly Authenticator: Propriete;
  public readonly SelectList: Propriete;
  public readonly DataBitmap: DataBitmap;

  public constructor() {
    super("MainBitmap", 7);
    /*
        NetworkId: 24
        IssuerId: 8
        ValidityEndDate: 14
        //PayMethod: 11
        Authenticator: 16
        SelectList: 32
        DataBitmap: 2
    */

    this.NetworkId = new NetworkPropriete("NetworkId", 24);
    this.IssuerId = new Propriete("IssuerId", 8);
    this.ValidityEndDate = new DatePropriete("ValidityEndDate", 14);
    this.PayMethod = new Propriete("PayMethod", 11);
    this.Authenticator = new Propriete("Authenticator", 16);
    this.SelectList = new Propriete("SelectList", 32);
    this.DataBitmap = new DataBitmap();

    this.proprietes.push(this.NetworkId);
    this.proprietes.push(this.IssuerId);
    this.proprietes.push(this.ValidityEndDate);
    this.proprietes.push(this.PayMethod);
    this.proprietes.push(this.Authenticator);
    this.proprietes.push(this.SelectList);
    this.proprietes.push(this.DataBitmap);
  }

  public toString(): string {
    return (
      "Réseau : " +
      this.NetworkId.getFormatedValue() +
      " [" +
      this.NetworkId.rawValue +
      "]" +
      "\n" +
      "Emetteur et créateur billétique : " +
      "[" +
      this.IssuerId.rawValue +
      "]" +
      "\n" +
      "Valide jusqu'au " +
      this.ValidityEndDate.getFormatedValue() +
      "\n" +
      "Méthode de payement : " +
      //this.PayMethod.getFormatedValue() +
      " [" +
      this.PayMethod.rawValue +
      "]" +
      "\n" +
      "Authentificator : " +
      //this.Authenticator.getFormatedValue() +
      " [" +
      this.Authenticator.rawValue +
      "]"
    );
  }
}
