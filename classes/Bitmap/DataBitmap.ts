import Bitmap from "../Type/Bitmap";
import Propriete from "../Type/Propriete";

export default class DataBitmap extends Bitmap {
  public readonly CardStatus: Propriete;
  public readonly Data2: Propriete;

  public constructor() {
    super("DataBitmap", 2);

    /*
        CardStatus: 1
        Data2: 1
    */

    this.CardStatus = new Propriete("CardStatus", 1);
    this.Data2 = new Propriete("Data2", 0);

    this.proprietes.push(this.CardStatus);
    this.proprietes.push(this.Data2);
  }
}
