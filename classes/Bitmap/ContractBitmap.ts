import Bitmap from "../Type/Bitmap";
import Propriete from "../Type/Propriete";
import CustomerInfoBitmap from "./CustomerInfoBitmap";
import PassengerInfoBitmap from "./PassengerInfoBitmap";
import ContractRestrictBitmap from "./ContractRestrictionBitmap";
import ValidityInfoBitmap from "./ValidityInfoBitmap";
import JourneyDataBitmap from "./JourneyDataBitmap";
import ContractSaleDataBitmap from "./ContractSaleDataBitmap";
import NetworkPropriete from "../Type/NetworkPropriete";
import TarifContractPropriete from "../Type/TarifContractPropriete";
import MontantPropriete from "../Type/MontantPropriete";

export default class ContractBitmap extends Bitmap {
  public readonly NetworkId: NetworkPropriete;
  public readonly Provider: Propriete;
  public readonly Tarif: TarifContractPropriete;
  public readonly SerialNumber: Propriete;
  public readonly CustomerBitmap: CustomerInfoBitmap;
  public readonly PassengerBitmap: PassengerInfoBitmap;
  public readonly VehicleClassAllowed: Propriete;
  public readonly PaymentPointer: Propriete;
  public readonly PayMethod: Propriete;
  public readonly Services: Propriete;
  public readonly PriceAmount: MontantPropriete;
  public readonly PriceUnit: Propriete;
  public readonly RestrictBitmap: ContractRestrictBitmap;
  public readonly ValidityInfoBitmap: ValidityInfoBitmap;
  public readonly JourneyDataBitmap: JourneyDataBitmap;
  public readonly ValiditySaleDataBitmap: ContractSaleDataBitmap;
  public readonly Status: Propriete;
  public readonly LoyaltyPoints: Propriete;
  public readonly ContractAuthenticator: Propriete;
  public readonly ContractData: Propriete;

  public constructor() {
    super("ContractBitmap", 20);

    /*
    NetworkId: 24,
    Provider: 8,
    Tarif: 16,
    SerialNumber: 32,
    //CustomerBitmap: 2,
    //PassengerInfoBitmap: 2,
    //VehicleClassAllowed: 6,
    //PaymentPointer: 32,
    PayMethod: 11,
    //Services: 16,
    PriceAmount: 16,
    //PriceUnit: 16,
    //RestrictBitmap: 7,
    ValidityInfoBitmap: 9,
    //JourneyDataBitmap: 8,
    ValiditySaleDataBitmap: 4,
    //Status: 8,
    //LoyaltyPoints: 16,
    ContractAuthenticator: 16,
    //ContractData(0..255): 0
        */
    this.NetworkId = new NetworkPropriete("NetworkId", 24);
    this.Provider = new Propriete("Provider", 8);
    this.Tarif = new TarifContractPropriete("Tarif", 16);
    this.SerialNumber = new Propriete("SerialNumber", 32);
    this.CustomerBitmap = new CustomerInfoBitmap();
    this.PassengerBitmap = new PassengerInfoBitmap();
    this.VehicleClassAllowed = new Propriete("VehicleClassAllowed", 6);
    this.PaymentPointer = new Propriete("PaymentPointer", 32);
    this.PayMethod = new Propriete("PayMethod", 11);
    this.Services = new Propriete("Services", 16);
    this.PriceAmount = new MontantPropriete("PriceAmount", 16);
    this.PriceUnit = new Propriete("PriceUnit", 16);
    this.RestrictBitmap = new ContractRestrictBitmap();
    this.ValidityInfoBitmap = new ValidityInfoBitmap();
    this.JourneyDataBitmap = new JourneyDataBitmap();
    this.ValiditySaleDataBitmap = new ContractSaleDataBitmap();
    this.Status = new Propriete("Status", 8);
    this.LoyaltyPoints = new Propriete("LoyaltyPoints", 16);
    this.ContractAuthenticator = new Propriete("ContractAuthenticator", 16);
    this.ContractData = new Propriete("ContractData", 0);

    this.proprietes.push(this.NetworkId);
    this.proprietes.push(this.Provider);
    this.proprietes.push(this.Tarif);
    this.proprietes.push(this.SerialNumber);
    this.proprietes.push(this.CustomerBitmap);
    this.proprietes.push(this.PassengerBitmap);
    this.proprietes.push(this.VehicleClassAllowed);
    this.proprietes.push(this.PaymentPointer);
    this.proprietes.push(this.PayMethod);
    this.proprietes.push(this.Services);
    this.proprietes.push(this.PriceAmount);
    this.proprietes.push(this.PriceUnit);
    this.proprietes.push(this.RestrictBitmap);
    this.proprietes.push(this.ValidityInfoBitmap);
    this.proprietes.push(this.JourneyDataBitmap);
    this.proprietes.push(this.ValiditySaleDataBitmap);
    this.proprietes.push(this.Status);
    this.proprietes.push(this.LoyaltyPoints);
    this.proprietes.push(this.ContractAuthenticator);
    this.proprietes.push(this.ContractData);
  }

  public toString(): string {
    return (
      "Réseau : " +
      this.NetworkId.getFormatedValue() +
      " [" +
      this.NetworkId.rawValue +
      "]" +
      "\n" +
      "Provider : " +
      //this.Provider.getFormatedValue() +
      " [" +
      this.Provider.rawValue +
      "]" +
      "\n" +
      "Tarif : " +
      this.Tarif.getFormatedValue() +
      " [" +
      this.Tarif.rawValue +
      "]" +
      "\n" +
      "Prix : " +
      this.PriceAmount.getFormatedValue() +
      " [" +
      this.PriceAmount.rawValue +
      "]" +
      "\n" +
      "Moyen de payement : " +
      //this.PayMethod.getFormatedValue() +
      " [" +
      this.PayMethod.rawValue +
      "]" +
      "\n" +
      "Classe autorisé : " +
      //this.VehicleClassAllowed.getFormatedValue() +
      " [" +
      this.VehicleClassAllowed.rawValue +
      "]" +
      "\n" +
      "Statut : " +
      //this.Status.getFormatedValue() +
      " [" +
      this.Status.rawValue +
      "]" +
      "\n" +
      "Points de fidélité : " +
      //this.LoyaltyPoints.getFormatedValue() +
      " [" +
      this.LoyaltyPoints.rawValue +
      "]" +
      "\n" +
      this.ValidityInfoBitmap.toString()
    );
  }

  public getPrimaryInfo(): string {
    return this.Tarif.getFormatedValue();
  }

  public getSecondaryInfo(): Array<string> {
    let infos = new Array<string>();
    if (this.PriceAmount.rawValue != 0)
      infos.push("Prix : " + this.PriceAmount.getFormatedValue());

    if (this.ValidityInfoBitmap)
      infos = infos.concat(this.ValidityInfoBitmap.getPrimaryInfo());

    return infos;
  }
}
