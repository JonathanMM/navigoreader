import Bitmap from "../Type/Bitmap";
import Propriete from "../Type/Propriete";
import DatePropriete from "../Type/DatePropriete";
import TimePropriete from "../Type/TimePropriete";

export default class EventDataBitmap extends Bitmap {
  public readonly DateFirstStamp: DatePropriete;
  public readonly TimeFirstStamp: TimePropriete;
  public readonly Simulation: Propriete;
  public readonly Trip: Propriete;
  public readonly RouteDirection: Propriete;

  public constructor() {
    super("EventDataBitmap", 5);

    /*
    DataDateFirstStamp: 14,
    DataTimeFirstStamp: 11,
    //DataSimulation: 1,
    //DataTrip: 2,
    //DataRouteDirection: 2
    */

    this.DateFirstStamp = new DatePropriete("DateFirstStamp", 14);
    this.TimeFirstStamp = new TimePropriete("TimeFirstStamp", 11);
    this.Simulation = new Propriete("Simulation", 1);
    this.Trip = new Propriete("Trip", 2);
    this.RouteDirection = new Propriete("RouteDirection", 2);

    this.proprietes.push(this.DateFirstStamp);
    this.proprietes.push(this.TimeFirstStamp);
    this.proprietes.push(this.Simulation);
    this.proprietes.push(this.Trip);
    this.proprietes.push(this.RouteDirection);
  }

  public toString(): string {
    return (
      "Première validation : " +
      this.DateFirstStamp.getFormatedValue() +
      " " +
      this.TimeFirstStamp.getFormatedValue()
    );
  }
}
