import Bitmap from "../Type/Bitmap";
import Propriete from "../Type/Propriete";
import DatePropriete from "../Type/DatePropriete";
import TimePropriete from "../Type/TimePropriete";
import ZonesPropriete from "../Type/ZonesPropriete";

export default class ValidityInfoBitmap extends Bitmap {
  public readonly StartDate: DatePropriete;
  public readonly StartTime: TimePropriete;
  public readonly EndDate: DatePropriete;
  public readonly EndTime: TimePropriete;
  public readonly Duration: Propriete;
  public readonly LimitDate: DatePropriete;
  public readonly Zones: ZonesPropriete;
  public readonly Journeys: Propriete;
  public readonly PeriodJourneys: Propriete;

  public constructor() {
    super("ValidityInfoBitmap", 9);

    /*
    StartDate: 14,
    StartTime: 11,
    EndDate: 14,
    EndTime: 11,
    //Duration: 8,
    LimitDate: 14,
    Zones: 8,
    //Journeys: 16,
    //PeriodJourneys: 8,
    */

    this.StartDate = new DatePropriete("StartDate", 14);
    this.StartTime = new TimePropriete("StartTime", 11);
    this.EndDate = new DatePropriete("EndDate", 14);
    this.EndTime = new TimePropriete("EndTime", 11);
    this.Duration = new Propriete("Duration", 8);
    this.LimitDate = new DatePropriete("LimitDate", 14);
    this.Zones = new ZonesPropriete("Zones", 8);
    this.Journeys = new Propriete("Journeys", 16);
    this.PeriodJourneys = new Propriete("PeriodJourneys", 8);

    this.proprietes.push(this.StartDate);
    this.proprietes.push(this.StartTime);
    this.proprietes.push(this.EndDate);
    this.proprietes.push(this.EndTime);
    this.proprietes.push(this.Duration);
    this.proprietes.push(this.LimitDate);
    this.proprietes.push(this.Zones);
    this.proprietes.push(this.Journeys);
    this.proprietes.push(this.PeriodJourneys);
  }

  public toString(): string {
    return (
      "Validité : du " +
      this.StartDate.getFormatedValue() +
      " " +
      this.StartTime.getFormatedValue() +
      " au " +
      this.EndDate.getFormatedValue() +
      " " +
      this.EndTime.getFormatedValue() +
      "\n" +
      "Durée : " +
      //this.Duration.getFormatedValue() +
      " [" +
      this.Duration.rawValue +
      "]" +
      "\n" +
      "Date limite d'utilisation : " +
      this.LimitDate.getFormatedValue() +
      "\n" +
      "Zone(s) : " +
      this.Zones.getFormatedValue() +
      " [" +
      this.Zones.rawValue +
      "]" +
      "\n" +
      "Trajets : " +
      //this.Journeys.getFormatedValue() +
      " [" +
      this.Journeys.rawValue +
      "]" +
      "\n" +
      "Période des trajets : " +
      //this.PeriodJourneys.getFormatedValue() +
      " [" +
      this.PeriodJourneys.rawValue +
      "]"
    );
  }

  public getPrimaryInfo(): Array<string> {
    let infos = new Array<string>();
    if (this.Zones.rawValue != 0)
      infos.push("Zones " + this.Zones.getFormatedValue());

    if (this.StartDate.rawValue != 0) {
      let infoValable;
      if (this.EndDate.rawValue != 0) {
        infoValable =
          "Valable du " +
          this.StartDate.getFormatedValue() +
          " au " +
          this.EndDate.getFormatedValue();
      } else {
        infoValable =
          "Valable à partir du " + this.StartDate.getFormatedValue();
      }
      infos.push(infoValable);
    }

    return infos;
  }
}
