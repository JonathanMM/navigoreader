import Bitmap from "../Type/Bitmap";
import Propriete from "../Type/Propriete";

export default class CustomerInfoBitmap extends Bitmap {
  public readonly Profile: Propriete;
  public readonly Number: Propriete;

  public constructor() {
    super("CustomerInfoBitmap", 2);

    /*
    //CustomerProfile: 6,
    //CustomerNumber: 32,
    */

    this.Profile = new Propriete("Profile", 6);
    this.Number = new Propriete("Number", 32);

    this.proprietes.push(this.Profile);
    this.proprietes.push(this.Number);
  }

  public toString(): string {
    return (
      "Profil client : " +
      this.Profile.getFormatedValue() +
      "\n" +
      "Numéro : " +
      this.Number.getFormatedValue()
    );
  }
}
