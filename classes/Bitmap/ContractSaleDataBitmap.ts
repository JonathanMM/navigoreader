import Bitmap from "../Type/Bitmap";
import Propriete from "../Type/Propriete";
import DatePropriete from "../Type/DatePropriete";
import TimePropriete from "../Type/TimePropriete";

export default class ContractSaleDataBitmap extends Bitmap {
  public readonly Date: DatePropriete;
  public readonly Time: TimePropriete;
  public readonly Agent: Propriete;
  public readonly Device: Propriete;

  public constructor() {
    super("ContractSaleData", 4);

    /*
    Date: 14,
    Time: 11,
    Agent: 8,
    Device: 16,
    */

    this.Date = new DatePropriete("Date", 14);
    this.Time = new TimePropriete("Time", 11);
    this.Agent = new Propriete("Agent", 8);
    this.Device = new Propriete("Device", 16);

    this.proprietes.push(this.Date);
    this.proprietes.push(this.Time);
    this.proprietes.push(this.Agent);
    this.proprietes.push(this.Device);
  }

  public toString(): string {
    return "Des informations de validités sont présentes";
  }
}
