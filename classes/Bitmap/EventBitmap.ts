import Bitmap from "../Type/Bitmap";
import Propriete from "../Type/Propriete";
import EventDataBitmap from "./EventDataBitmap";
import NetworkPropriete from "../Type/NetworkPropriete";
import MontantPropriete from "../Type/MontantPropriete";
import EventCodePropriete from "../Type/EventCodePropriete";
import ExploitantPropriete from "../Type/ExploitantPropriete";
import Station from "../Data/Station";
import EventResultPropriete from "../Type/EventResultPropriete";

export default class EventBitmap extends Bitmap {
  public readonly DisplayData: Propriete;
  public readonly NetworkId: NetworkPropriete;
  public readonly Code: EventCodePropriete;
  public readonly Result: EventResultPropriete;
  public readonly ServiceProvider: ExploitantPropriete;
  public readonly NotokCounter: Propriete;
  public readonly SerialNumber: Propriete;
  public readonly Destination: Propriete;
  public readonly LocationId: Propriete;
  public readonly LocationGate: Propriete;
  public readonly Device: Propriete;
  public readonly RouteNumber: Propriete;
  public readonly RouteVariante: Propriete;
  public readonly JourneyRun: Propriete;
  public readonly VehicleId: Propriete;
  public readonly VehicleClass: Propriete;
  public readonly LocationType: Propriete;
  public readonly Employee: Propriete;
  public readonly LocationReference: Propriete;
  public readonly JourneyInterchanges: Propriete;
  public readonly PeriodJourneys: Propriete;
  public readonly TotalJourneys: Propriete;
  public readonly JourneyDistance: Propriete;
  public readonly PriceAmount: MontantPropriete;
  public readonly PriceUnit: Propriete;
  public readonly ContractPointer: Propriete;
  public readonly Authenticator: Propriete;
  public readonly EventDataBitmap: EventDataBitmap;

  private readonly _stationProvider: Station = new Station();

  public constructor() {
    super("EventBitmap", 28);

    /*
    //DisplayData: 8,
    NetworkId : 24,
    Code: 8,
    Result: 8,
    ServiceProvider: 8,
    //NotokCounter: 8,
    //SerialNumber: 24,
    //Destination: 16,
    LocationId: 16,
    //LocationGate: 8,
    Device: 16,
    RouteNumber: 16,
    //RouteVariante: 8,
    JourneyRun: 16,
    VehicleId: 16,
    //VehicleClass: 8
    //LocationType: 5,
    //Employee: 240,
    //LocationReference: 16
    //JourneyInterchanges: 8,
    //PeriodJourneys: 16,
    //TotalJourneys: 16,
    //JourneyDistance: 16,
    //PriceAmount: 16,
    //PriceUnit: 16,
    ContractPointer: 5,
    //Authenticator: 16,
    EventDataBitmap: 5
    */

    this.DisplayData = new Propriete("DisplayData", 8);
    this.NetworkId = new NetworkPropriete("NetworkId", 24);
    this.Code = new EventCodePropriete("Code", 8);
    this.Result = new EventResultPropriete("Result", 8);
    this.ServiceProvider = new ExploitantPropriete("ServiceProvider", 8);
    this.NotokCounter = new Propriete("NotokCounter", 8);
    this.SerialNumber = new Propriete("SerialNumber", 24);
    this.Destination = new Propriete("Destination", 16);
    this.LocationId = new Propriete("LocationId", 16);
    this.LocationGate = new Propriete("LocationGate", 8);
    this.Device = new Propriete("Device", 16);
    this.RouteNumber = new Propriete("RouteNumber", 16);
    this.RouteVariante = new Propriete("RouteVariante", 8);
    this.JourneyRun = new Propriete("JourneyRun", 16);
    this.VehicleId = new Propriete("VehicleId", 16);
    this.VehicleClass = new Propriete("VehicleClass", 8);
    this.LocationType = new Propriete("LocationType", 5);
    this.Employee = new Propriete("Employee", 240);
    this.LocationReference = new Propriete("LocationReference", 16);
    this.JourneyInterchanges = new Propriete("JourneyInterchanges", 8);
    this.PeriodJourneys = new Propriete("PeriodJourneys", 16);
    this.TotalJourneys = new Propriete("TotalJourneys", 16);
    this.JourneyDistance = new Propriete("JourneyDistance", 16);
    this.PriceAmount = new MontantPropriete("PriceAmount", 16);
    this.PriceUnit = new Propriete("PriceUnit", 16);
    this.ContractPointer = new Propriete("ContractPointer", 5);
    this.Authenticator = new Propriete("Authenticator", 16);
    this.EventDataBitmap = new EventDataBitmap();

    this.proprietes.push(this.DisplayData);
    this.proprietes.push(this.NetworkId);
    this.proprietes.push(this.Code);
    this.proprietes.push(this.Result);
    this.proprietes.push(this.ServiceProvider);
    this.proprietes.push(this.NotokCounter);
    this.proprietes.push(this.SerialNumber);
    this.proprietes.push(this.Destination);
    this.proprietes.push(this.LocationId);
    this.proprietes.push(this.LocationGate);
    this.proprietes.push(this.Device);
    this.proprietes.push(this.RouteNumber);
    this.proprietes.push(this.RouteVariante);
    this.proprietes.push(this.JourneyRun);
    this.proprietes.push(this.VehicleId);
    this.proprietes.push(this.VehicleClass);
    this.proprietes.push(this.LocationType);
    this.proprietes.push(this.Employee);
    this.proprietes.push(this.LocationReference);
    this.proprietes.push(this.JourneyInterchanges);
    this.proprietes.push(this.PeriodJourneys);
    this.proprietes.push(this.TotalJourneys);
    this.proprietes.push(this.JourneyDistance);
    this.proprietes.push(this.PriceAmount);
    this.proprietes.push(this.PriceUnit);
    this.proprietes.push(this.ContractPointer);
    this.proprietes.push(this.Authenticator);
    this.proprietes.push(this.EventDataBitmap);
  }

  public toString(): string {
    return (
      "Réseau : " +
      this.NetworkId.getFormatedValue() +
      " [" +
      this.NetworkId.rawValue +
      "]" +
      "\n" +
      "Type d'événement : " +
      this.Code.getFormatedValue() +
      " [" +
      this.Code.rawValue +
      "]" +
      "\n" +
      "Retour : " +
      this.Result.getFormatedValue() +
      " [" +
      this.Result.rawValue +
      "]" +
      "\n" +
      "Exploitant : " +
      this.ServiceProvider.getFormatedValue() +
      " [" +
      this.ServiceProvider.rawValue +
      "]" +
      "\n" +
      "Station : " +
      this._stationProvider.getStation(
        this.Code.rawValue,
        this.LocationId.rawValue
      ) +
      " [" +
      this.LocationId.rawValue +
      "]" +
      "\n" +
      "Terminal de validation : " +
      //this.Device.getFormatedValue() +
      " [" +
      this.Device.rawValue +
      "]" +
      "\n" +
      "Ligne : " +
      //this.RouteNumber.getFormatedValue() +
      " [" +
      this.RouteNumber.rawValue +
      "]" +
      "\n" +
      "Véhicule : " +
      //this.VehicleId.getFormatedValue() +
      " [" +
      this.VehicleId.rawValue +
      "]" +
      "\n" +
      "Destination : " +
      //this.Destination.getFormatedValue() +
      " [" +
      this.Destination.rawValue +
      "]" +
      "\n" +
      "Prix : " +
      this.PriceAmount.getFormatedValue() +
      " [" +
      this.PriceAmount.rawValue +
      "]" +
      "\n" +
      "Nombre de correspondances : " +
      //this.JourneyInterchanges.getFormatedValue() +
      " [" +
      this.JourneyInterchanges.rawValue +
      "]" +
      "\n" +
      "Distance : " +
      //this.JourneyDistance.getFormatedValue() +
      " [" +
      this.JourneyDistance.rawValue +
      "]" +
      "\n" +
      this.EventDataBitmap.toString()
    );
  }

  public getPrimaryInfo(): string {
    return this.Code.getFormatedValue();
  }

  public getSecondaryInfo(): Array<string> {
    let infos = new Array<string>();

    if (this.Result.rawValue != 0)
      infos.push("Incident : " + this.Result.getFormatedValue());

    infos.push(
      "Station : " +
        this._stationProvider.getStation(
          this.Code.rawValue,
          this.LocationId.rawValue
        ) +
        " − Réseau : " +
        this.ServiceProvider.getFormatedValue()
    );

    return infos;
  }
}
