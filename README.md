navigo reader
=============

Lecteur (basique) de passe navigo en react-native sur android.

## Fonctionnalités

* Affiche les forfaits/tickets de votre carte
* Affiche les 3 dernières validations
* Affiche les 3 derniers incidents stockés sur la carte

## Remerciement

Ce projet se base sur tout le travail d'élageage et de documentation des projets :
* [Cardpeek](https://github.com/L1L1/cardpeek)
* [Navigoat](https://github.com/sp1d3rb0y/Navigoat/)

## Détection des stations, exploitants, etc…

Comme il n'existe pas de liste officielle permettant de faire la correspondance entre les codes stockés sur la carte, une table de mapping emprunté des précédents projets est utilisé. En cas d'information inconnue, un code est donné par l'application. En ouvrant un ticket, ce code sera intérgré aux futurs versions afin d'alimenter les données.

## Générer un apk

Pour générer un apk, il est nécessaire de mettre à jour son numéro de version, dans app.json et android/app/build.gradle
puis, pour lancer la compilation, il est nécessaire de faire :
* Depuis la racine du projet
```bash
react-native start
```
* et depuis le dossier android :
```bash
./gradlew assembleRelease
```
L'apk généré se trouvera alors à l'adresse android/app/build/outputs/release/app-release.apk.

Pour lancer l'application en mode debug,
À la racine du projet, faire
```bash
react-native start
```
puis
```bash
npm run android
```

Un ```npm run android -- --variant=release``` permet de faire installer l'apk en mode release sur un appareil connecté via adb 

## Notes

* C'est du react-native, donc potentiellement exportable sur iOS également sans difficulté
* Le passe navigo utilise le standard Calypso, donc l'application devrait savoir lire toute autre carte utilisant ce même protocole