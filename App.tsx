import React from "react";
import { SafeAreaView, ScrollView, View, Text } from "react-native";
import NfcManager, { NfcTech, TagEvent } from "react-native-nfc-manager";
import NavigoReader from "./classes/NavigoReader";
import Navigo from "./classes/Navigo";
import NavigoDisplayer from "./vue/Steps/NavigoDisplayer";
import InitAppDisplayer from "./vue/Steps/InitAppDisplayer";
import TitleApplication from "./vue/TitleApplication";
import Tag from "./classes/Data/Tag";
import BoutonsLecteur from "./vue/BoutonsLecteur";
import WaitingNavigoDisplayer from "./vue/Steps/WaitingNavigoDisplayer";
import ReadingNavigoDisplayer from "./vue/Steps/ReadingNavigoDisplayer";
import NfcNotSupportedDisplayed from "./vue/Steps/NfcNotSupportedDisplayed";
import ContentView from "./vue/ContentView";

enum AppStep {
  Start,
  WaitingCard,
  ReadingCard,
  DisplayInfo,
  NfcNotsupported,
}

interface AppProps {}

interface AppState {
  navigo: Navigo | null;
  debugMode: boolean;
  step: AppStep;
  etat: string;
}

export default class App extends React.Component<AppProps, AppState> {
  public constructor(props: AppProps) {
    super(props);
    this.state = {
      navigo: null,
      debugMode: false,
      etat: "",
      step: AppStep.Start,
    };
  }

  componentDidMount() {
    NfcManager.start();
    if (!NfcManager.isSupported())
      this.setState({ ...this.state, step: AppStep.NfcNotsupported });
  }

  private async lirePasse(tagEvent?: TagEvent) {
    console.info("Passe vu");
    await NfcManager.requestTechnology(NfcTech.IsoDep);
    this.setState({ ...this.state, step: AppStep.ReadingCard });
    console.warn("Techno demandé");
    this.setState({ ...this.state, etat: "Techno demandé" });

    const navigoReader = new NavigoReader();
    let tag: Tag;
    if (tagEvent == undefined) {
      tag = await NfcManager.getTag();
    } else {
      tag = new Tag();
      if (tagEvent.id && tagEvent.id.length > 0)
        tag.id = tagEvent.id.toString();
    }
    let navigo = await navigoReader.readNavigo(tag);

    console.warn("Navigo", navigo);
    this.setState({ ...this.state, etat: "Navigo lu" });

    this.setState({ ...this.state, navigo: navigo, step: AppStep.DisplayInfo });

    this._cancel();
  }

  componentWillUnmount() {
    this._cancel();
  }

  private _cancel() {
    NfcManager.unregisterTagEvent().catch(() => 0);
    NfcManager.cancelTechnologyRequest().catch(() => 0);
  }

  private _reset() {
    this.setState({ ...this.state, navigo: null, step: AppStep.Start });
    this._cancel();
  }

  private async _test() {
    try {
      console.info("Attente d'un passe");
      this.setState({
        ...this.state,
        etat: "Attente d'un passe",
        step: AppStep.WaitingCard,
      });
      this.lirePasse();
    } catch (ex) {
      console.error("ex", ex);
      this._cancel();
    }
  }

  public toggleDebugMode() {
    this.setState({ ...this.state, debugMode: !this.state.debugMode });
  }

  public displayCurrentStep() {
    switch (this.state.step) {
      case AppStep.Start:
        return <InitAppDisplayer />;
      case AppStep.DisplayInfo:
        if (this.state.navigo == null) return <InitAppDisplayer />;
        return (
          <NavigoDisplayer
            navigo={this.state.navigo}
            debugMode={this.state.debugMode}
          />
        );
      case AppStep.WaitingCard:
        return <WaitingNavigoDisplayer />;
      case AppStep.ReadingCard:
        return <ReadingNavigoDisplayer />;
      case AppStep.NfcNotsupported:
        return <NfcNotSupportedDisplayed />;
    }
  }

  render() {
    return (
      <SafeAreaView>
        <View
          style={{
            width: 100,
            height: "100%",
            backgroundColor: "#00BFF1",
            zIndex: 0,
          }}
        ></View>
        <ScrollView
          style={{
            paddingRight: 10,
            width: "100%",
            height: "100%",
            zIndex: 1,
            position: "absolute",
          }}
        >
          <TitleApplication onPressIcon={() => this.toggleDebugMode()} />
          <BoutonsLecteur
            lirePasse={() => this._test()}
            remiseZero={() => this._reset()}
          />
          {this.state.debugMode && (
            <ContentView>
              <Text>État : {this.state.etat}</Text>
            </ContentView>
          )}
          {this.displayCurrentStep()}
        </ScrollView>
      </SafeAreaView>
    );
  }
}
